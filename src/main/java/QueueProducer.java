import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class QueueProducer {

    private String brokerUrl;
    private String username;
    private String password;

    public QueueProducer(String brokerUrl, String username, String password) {
        this.brokerUrl = brokerUrl;
        this.username = username;
        this.password = password;
    }

    public void sendMessage(final String queueName, final String msg) throws JMSException {
        // Get the connection factory
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(username, password, brokerUrl);

        // Get the effective connection
        Connection connection = connectionFactory.createConnection();

        // Start the connection
        connection.start();

        // Create a session
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        // Create a queue if it does not exist
        Destination queue = session.createQueue(queueName);

        // Create a producer
        MessageProducer producer = session.createProducer(queue);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        // Create message
        TextMessage textMessage = session.createTextMessage(msg);

        // Send message to the queue
        producer.send(textMessage);

        // Close connection and session
        session.close();
        connection.close();
    }

}
