
public class ProducerApp {

    public static void main(String[] args) throws Exception{
        QueueProducer queueProducer = new QueueProducer("tcp://localhost:61616", "guest", "guest");

        for(int i = 0; i < 10; i++){
            queueProducer.sendMessage("test.queue1", "MSG1 " + i);
            queueProducer.sendMessage("test.queue2", "MSG2 " + i);
            queueProducer.sendMessage("test.queue3", "MSG3 " + i);
        }

    }

}
